package fio

import (
	"context"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

func TestRun(t *testing.T) {
	runner, err := NewFioRunner(0, "fio-rand-RW.fio")
	if err != nil {
		t.Fatalf("Can't create runner: %v", err)
	}
	ready := make(chan *Datum)
	runner.AddWatcher(func(d *Datum) { ready <- d })
	done := make(chan struct{})
	runner.AddFinisher(done)
	cancel, err := runner.Start(context.Background())
	defer func() {
		(*cancel)()
		outdir, err := os.Open("/tmp")
		if err != nil {
			t.Fatalf("Can't open /tmp to clean up test files: %v", err)
		}
		names, err := outdir.Readdirnames(0)
		if err != nil {
			t.Fatalf("Can't read /tmp/ to clean up test files: %v", err)
		}
		filecount := 0
		for _, name := range names {
			if strings.HasPrefix(name, "fiodata-") {
				filecount += 1
				os.Remove(filepath.Join("/tmp", name))
			}
		}
		if filecount != 4 {
			t.Errorf("Expected 4 temp files, found %d", filecount)
		}
	}()
	if err != nil {
		t.Fatalf("Can't start runner: %v", err)
	}
	d := <-ready
	if d.HasDataError {
		t.Errorf("Bad data 1: %v", d.DataError)
	}
	d = <-ready
	if d.HasDataError {
		t.Errorf("Bad data 2: %v", d.DataError)
	}

	// Wait for fio to be done.
	<-done
}
