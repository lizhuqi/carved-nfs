package driver

import (
	"fmt"

	"github.com/container-storage-interface/spec/lib/go/csi"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	apiv1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/util/retry"
	"k8s.io/klog"
)

const (
	// MaxVolumeSizeInBytes is the maximum standard and ssd size of 64TB
	MaxVolumeSizeInBytes     int64 = 64 * 1024 * 1024 * 1024 * 1024
	MinimumVolumeSizeInBytes int64 = 1 * 1024 * 1024 * 1024
	MinimumDiskSizeInGb            = 1
)

type controllerServer struct {
	clientset *kubernetes.Clientset
}

func NewControllerServer() (csi.ControllerServer, error) {
	config, err := rest.InClusterConfig()
	if err != nil {
		return nil, err
	}
	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}
	return &controllerServer{clientset}, nil
}

func (s *controllerServer) CreateVolume(ctx context.Context, req *csi.CreateVolumeRequest) (*csi.CreateVolumeResponse, error) {
	klog.V(4).Infof("CreateVolume called with request %v", *req)

	volumeId := req.GetName()
	if volumeId == "" {
		return nil, status.Error(codes.InvalidArgument, "CreateVolume name must be provided")
	}

	if err := validateCapabilities(req.GetVolumeCapabilities()); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	exists, err := s.volumeServerExists(ctx, volumeId)
	if err != nil {
		return nil, status.Error(codes.Unknown, err.Error())
	}
	if exists {
		return &csi.CreateVolumeResponse{Volume: &csi.Volume{VolumeId: volumeId}}, nil
	}

	reqBytes, err := getRequestCapacity(req.GetCapacityRange())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Bad capacity %v: %v", req.GetCapacityRange(), err))
	}
	if err = s.createVolumeServer(ctx, volumeId, reqBytes); err != nil {
		return nil, status.Error(codes.Unknown, err.Error())
	}

	return &csi.CreateVolumeResponse{Volume: &csi.Volume{VolumeId: volumeId}}, nil
}

func (s *controllerServer) DeleteVolume(ctx context.Context, req *csi.DeleteVolumeRequest) (*csi.DeleteVolumeResponse, error) {
	volumeId := req.GetVolumeId()
	if volumeId == "" {
		return nil, status.Error(codes.InvalidArgument, "volume id is empty")
	}
	if err := s.deleteVolumeServer(ctx, volumeId); err != nil {
		return nil, status.Error(codes.Unknown, err.Error())
	}

	return &csi.DeleteVolumeResponse{}, nil
}

func (s *controllerServer) ListVolumes(ctx context.Context, req *csi.ListVolumesRequest) (*csi.ListVolumesResponse, error) {
	if req.MaxEntries < 0 {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf(
			"ListVolumes got negative max entries %d", req.MaxEntries))
	}

	volumes, err := s.listVolumes(ctx)
	if err != nil {
		return nil, status.Error(codes.Unknown, err.Error())
	}
	entries := []*csi.ListVolumesResponse_Entry{}
	for _, v := range volumes {
		entries = append(entries, &csi.ListVolumesResponse_Entry{
			Volume: &csi.Volume{
				VolumeId: v,
			},
		})
	}
	return &csi.ListVolumesResponse{
		Entries: entries,
	}, nil
}

func (s *controllerServer) GetCapacity(ctx context.Context, req *csi.GetCapacityRequest) (*csi.GetCapacityResponse, error) {
	// This should return the capacity of the provisioning storage pool, which we'd have to get from the PD API.
	return nil, status.Error(codes.Unimplemented, "method GetCapacity not implemented")
}

func (s *controllerServer) ControllerExpandVolume(ctx context.Context, req *csi.ControllerExpandVolumeRequest) (*csi.ControllerExpandVolumeResponse, error) {
	volumeId := req.GetVolumeId()
	if volumeId == "" {
		return nil, status.Error(codes.InvalidArgument, "Missing volume")
	}
	reqBytes, err := getRequestCapacity(req.GetCapacityRange())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Invalid capacity range: %v", err))
	}
	pvcName, err := s.getVolumePVCName(ctx, volumeId)
	if err != nil {
		return nil, status.Error(codes.Unknown, err.Error())
	}
	client := s.clientset.CoreV1().PersistentVolumeClaims(driverNamespace)
	pvc, err := client.Get(ctx, pvcName, metav1.GetOptions{})
	if err != nil {
		return nil, status.Error(codes.Unknown, err.Error())
	}
	currBytes, err := getPVCCapacity(pvc)
	if err != nil {
		return nil, status.Error(codes.Unknown, fmt.Sprintf("Can't get capacity for %s: %v", volumeId, err))
	}
	if reqBytes <= currBytes {
		return &csi.ControllerExpandVolumeResponse{
			CapacityBytes:         currBytes,
			NodeExpansionRequired: false,
		}, nil
	}
	err = retry.RetryOnConflict(retry.DefaultRetry, func() error {
		pvc, getErr := client.Get(ctx, pvcName, metav1.GetOptions{})
		if getErr != nil {
			return getErr
		}
		pvc.Spec.Resources.Requests["storage"] = *resource.NewScaledQuantity(reqBytes, 0)
		_, updateErr := client.Update(ctx, pvc, metav1.UpdateOptions{})
		return updateErr
	})
	if err != nil {
		return nil, status.Error(codes.Unknown, err.Error())
	}
	return &csi.ControllerExpandVolumeResponse{
		CapacityBytes:         reqBytes,
		NodeExpansionRequired: false,
	}, nil
}

func (s *controllerServer) ValidateVolumeCapabilities(ctx context.Context, req *csi.ValidateVolumeCapabilitiesRequest) (*csi.ValidateVolumeCapabilitiesResponse, error) {
	volumeId := req.GetVolumeId()
	if volumeId == "" {
		return nil, status.Error(codes.InvalidArgument, "volume id is empty")
	}
	if err := validateCapabilities(req.GetVolumeCapabilities()); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	inst, err := newTempInstance(volumeId)
	if err != nil {
		return nil, status.Error(codes.Unknown, err.Error())
	}
	// Otherwise volume seems to work.
	inst.disconnect()

	return &csi.ValidateVolumeCapabilitiesResponse{
		Confirmed: &csi.ValidateVolumeCapabilitiesResponse_Confirmed{
			VolumeContext:      req.GetVolumeContext(),
			VolumeCapabilities: req.GetVolumeCapabilities(),
			Parameters:         req.GetParameters(),
		},
	}, nil
}

func (s *controllerServer) ControllerGetCapabilities(ctx context.Context, req *csi.ControllerGetCapabilitiesRequest) (*csi.ControllerGetCapabilitiesResponse, error) {
	makeCap := func(cap csi.ControllerServiceCapability_RPC_Type) *csi.ControllerServiceCapability {
		return &csi.ControllerServiceCapability{
			Type: &csi.ControllerServiceCapability_Rpc{
				Rpc: &csi.ControllerServiceCapability_RPC{
					Type: cap,
				},
			},
		}
	}

	return &csi.ControllerGetCapabilitiesResponse{
		Capabilities: []*csi.ControllerServiceCapability{
			makeCap(csi.ControllerServiceCapability_RPC_CREATE_DELETE_VOLUME),
			makeCap(csi.ControllerServiceCapability_RPC_LIST_VOLUMES),
			makeCap(csi.ControllerServiceCapability_RPC_GET_CAPACITY),
			makeCap(csi.ControllerServiceCapability_RPC_EXPAND_VOLUME),
		},
	}, nil
}

func getPVCCapacity(pvc *apiv1.PersistentVolumeClaim) (int64, error) {
	capacity, found := pvc.Spec.Resources.Requests["storage"]
	if !found {
		return 0, fmt.Errorf("No capacity found")
	}
	bytes, ok := capacity.AsDec().Unscaled()
	if !ok {
		return 0, fmt.Errorf("Capacity overflow %v", capacity)
	}
	return bytes, nil
}

func getRequestCapacity(capRange *csi.CapacityRange) (int64, error) {
	var capBytes int64
	// Default case where nothing is set
	if capRange == nil {
		capBytes = MinimumVolumeSizeInBytes
		return capBytes, nil
	}

	rBytes := capRange.GetRequiredBytes()
	rSet := rBytes > 0
	lBytes := capRange.GetLimitBytes()
	lSet := lBytes > 0

	if lSet && rSet && lBytes < rBytes {
		return 0, fmt.Errorf("Limit bytes %v is less than required bytes %v", lBytes, rBytes)
	}
	if lSet && lBytes < MinimumVolumeSizeInBytes {
		return 0, fmt.Errorf("Limit bytes %v is less than minimum volume size: %v", lBytes, MinimumVolumeSizeInBytes)
	}

	// If Required set just set capacity to that which is Required
	if rSet {
		capBytes = rBytes
	}

	// Limit is more than Required, but larger than Minimum. So we just set capcity to Minimum
	// Too small, default
	if capBytes < MinimumVolumeSizeInBytes {
		capBytes = MinimumVolumeSizeInBytes
	}
	return capBytes, nil
}

func (s *controllerServer) ControllerPublishVolume(ctx context.Context, req *csi.ControllerPublishVolumeRequest) (*csi.ControllerPublishVolumeResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ControllerPublishVolume not implemented")
}

func (s *controllerServer) ControllerUnpublishVolume(ctx context.Context, req *csi.ControllerUnpublishVolumeRequest) (*csi.ControllerUnpublishVolumeResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ControllerUnpublishVolume not implemented")
}

func (s *controllerServer) CreateSnapshot(context.Context, *csi.CreateSnapshotRequest) (*csi.CreateSnapshotResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateSnapshot not implemented")
}

func (s *controllerServer) DeleteSnapshot(context.Context, *csi.DeleteSnapshotRequest) (*csi.DeleteSnapshotResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteSnapshot not implemented")
}

func (s *controllerServer) ListSnapshots(context.Context, *csi.ListSnapshotsRequest) (*csi.ListSnapshotsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListSnapshots not implemented")
}

func (s *controllerServer) ControllerGetVolume(context.Context, *csi.ControllerGetVolumeRequest) (*csi.ControllerGetVolumeResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ControllerGetVolume not implemented")
}
