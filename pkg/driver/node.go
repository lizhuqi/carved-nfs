package driver

import (
	"fmt"
	"os"
	"strings"

	"cloud.google.com/go/compute/metadata"
	"github.com/container-storage-interface/spec/lib/go/csi"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"k8s.io/klog"
)

const (
	volumeLimitSmall int64 = 15
	volumeLimitBig   int64 = 127
)

type nodeServer struct{}

func NewNodeServer() (csi.NodeServer, error) {
	return &nodeServer{}, nil
}

func (n *nodeServer) NodeGetInfo(context.Context, *csi.NodeGetInfoRequest) (*csi.NodeGetInfoResponse, error) {
	project, err := metadata.ProjectID()
	if err != nil {
		return nil, fmt.Errorf("Can't get project id: %w", err)
	}
	zone, err := metadata.Zone()
	if err != nil {
		return nil, fmt.Errorf("Can't get zone: %w", err)
	}

	instance, err := metadata.InstanceName()
	if err != nil {
		return nil, fmt.Errorf("Can't get instance: %w", err)
	}

	nodeId := fmt.Sprintf("%s/%s/%s", project, zone, instance)
	limits, err := n.getVolumeLimits()
	return &csi.NodeGetInfoResponse{
		NodeId:            nodeId,
		MaxVolumesPerNode: limits,
	}, err
}

func (n *nodeServer) NodeStageVolume(ctx context.Context, req *csi.NodeStageVolumeRequest) (*csi.NodeStageVolumeResponse, error) {
	volumeId := req.GetVolumeId()
	if volumeId == "" {
		return nil, status.Error(codes.InvalidArgument, "missing volume id")
	}
	stagingTargetPath := req.GetStagingTargetPath()
	if stagingTargetPath == "" {
		return nil, status.Error(codes.InvalidArgument, "missing staging target path")
	}
	volumeCapability := req.GetVolumeCapability()
	if volumeCapability == nil {
		return nil, status.Error(codes.InvalidArgument, "missing volume capability")
	}
	if err := validateCapabilities([]*csi.VolumeCapability{volumeCapability}); err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("invalid capability: %v", err))
	}

	nfs := NewNFS(serviceName(volumeId), "/")
	alreadyMounted, err := nfs.IsMaybeMounted(stagingTargetPath)
	// Note stagingTargetPath may not have been created depending on the version of the kubelet.
	if err != nil && !os.IsNotExist(err) {
		return nil, status.Error(codes.Unknown, fmt.Sprintf("error validating mount point %s: %v", stagingTargetPath, err))
	}
	if alreadyMounted {
		return &csi.NodeStageVolumeResponse{}, nil
	}
	if os.IsNotExist(err) {
		if err := os.MkdirAll(stagingTargetPath, 0750); err != nil {
			return nil, status.Error(codes.Unknown, fmt.Sprintf("failed to mkdir %s", stagingTargetPath))
		}
	}
	if err := nfs.Mount(stagingTargetPath); err != nil {
		return nil, status.Error(codes.Unknown, err.Error())
	}
	return &csi.NodeStageVolumeResponse{}, nil
}

func (n *nodeServer) NodeUnstageVolume(ctx context.Context, req *csi.NodeUnstageVolumeRequest) (*csi.NodeUnstageVolumeResponse, error) {
	volumeId := req.GetVolumeId()
	if volumeId == "" {
		return nil, status.Error(codes.InvalidArgument, "missing volume id")
	}
	stagingTargetPath := req.GetStagingTargetPath()
	if stagingTargetPath == "" {
		return nil, status.Error(codes.InvalidArgument, "missing staging target path")
	}

	nfs := NewNFS(serviceName(volumeId), "/")
	err := nfs.UnMount(stagingTargetPath)
	if err != nil {
		klog.Warningf("Unount error during unstage (ignored): %v", err)
	}
	os.Remove(stagingTargetPath)
	return &csi.NodeUnstageVolumeResponse{}, nil
}

func (n *nodeServer) NodePublishVolume(ctx context.Context, req *csi.NodePublishVolumeRequest) (*csi.NodePublishVolumeResponse, error) {
	volumeId := req.GetVolumeId()
	if volumeId == "" {
		return nil, status.Error(codes.InvalidArgument, "missing volume id")
	}
	targetPath := req.GetTargetPath()
	if targetPath == "" {
		return nil, status.Error(codes.InvalidArgument, "missing target path")
	}
	stagingTargetPath := req.GetStagingTargetPath()
	if stagingTargetPath == "" {
		return nil, status.Error(codes.InvalidArgument, "missing staging target path")
	}
	volumeCapability := req.GetVolumeCapability()
	if volumeCapability == nil {
		return nil, status.Error(codes.InvalidArgument, "missing volume capability")
	}
	if err := validateCapabilities([]*csi.VolumeCapability{volumeCapability}); err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("invalid capability: %v", err))
	}

	nfs := NewNFS(serviceName(volumeId), "/")
	alreadyMounted, err := nfs.IsMaybeMounted(targetPath)
	if err != nil && !os.IsNotExist(err) {
		return nil, status.Error(codes.Unknown, fmt.Sprintf("error validating mount point %s: %v", stagingTargetPath, err))
	}
	if alreadyMounted {
		return &csi.NodePublishVolumeResponse{}, nil
	}
	if os.IsNotExist(err) {
		if err := os.MkdirAll(targetPath, 0750); err != nil {
			return nil, status.Error(codes.Unknown, fmt.Sprintf("failed to mkdir %s", targetPath))
		}
	}
	err = nfs.BindMount(stagingTargetPath, targetPath, req.GetReadonly())
	if err != nil {
		os.Remove(targetPath)
		return nil, status.Error(codes.Unknown, fmt.Sprintf("bind mount failed: %v", err))
	}
	return &csi.NodePublishVolumeResponse{}, nil
}

func (n *nodeServer) NodeUnpublishVolume(ctx context.Context, req *csi.NodeUnpublishVolumeRequest) (*csi.NodeUnpublishVolumeResponse, error) {
	volumeId := req.GetVolumeId()
	if volumeId == "" {
		return nil, status.Error(codes.InvalidArgument, "missing volume id")
	}
	targetPath := req.GetTargetPath()
	if targetPath == "" {
		return nil, status.Error(codes.InvalidArgument, "missing target path")
	}
	nfs := NewNFS(serviceName(volumeId), "/")
	if err := nfs.UnMount(targetPath); err != nil {
		return nil, status.Error(codes.Unknown, fmt.Sprintf("failed to unmount %s: %v", targetPath, err))
	}
	os.Remove(targetPath)
	return &csi.NodeUnpublishVolumeResponse{}, nil
}

func (n *nodeServer) NodeGetCapabilities(ctx context.Context, req *csi.NodeGetCapabilitiesRequest) (*csi.NodeGetCapabilitiesResponse, error) {
	makeCap := func(cap csi.NodeServiceCapability_RPC_Type) *csi.NodeServiceCapability {
		return &csi.NodeServiceCapability{
			Type: &csi.NodeServiceCapability_Rpc{
				Rpc: &csi.NodeServiceCapability_RPC{
					Type: cap,
				},
			},
		}
	}

	return &csi.NodeGetCapabilitiesResponse{
		Capabilities: []*csi.NodeServiceCapability{
			makeCap(csi.NodeServiceCapability_RPC_STAGE_UNSTAGE_VOLUME),
		},
	}, nil
}

func (n *nodeServer) getVolumeLimits() (int64, error) {
	// Machine-type format: n1-type-CPUS or custom-CPUS-RAM or f1/g1-type
	fullMachineType, err := metadata.Get("instance/machine-type")
	if err != nil {
		return 0, fmt.Errorf("Can't get machine type: %w", err)
	}
	// Response format: "projects/[NUMERIC_PROJECT_ID]/machineTypes/[MACHINE_TYPE]"
	splits := strings.Split(fullMachineType, "/")
	machineType := splits[len(splits)-1]

	smallMachineTypes := []string{"f1-micro", "g1-small", "e2-micro", "e2-small", "e2-medium"}
	for _, st := range smallMachineTypes {
		if machineType == st {
			return volumeLimitSmall, nil
		}
	}
	return volumeLimitBig, nil
}

func (s *nodeServer) NodeExpandVolume(context.Context, *csi.NodeExpandVolumeRequest) (*csi.NodeExpandVolumeResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method NodeExpandVolume not implemented")
}

func (s *nodeServer) NodeGetVolumeStats(context.Context, *csi.NodeGetVolumeStatsRequest) (*csi.NodeGetVolumeStatsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method NodeGetVolumeStats not implemented")
}
