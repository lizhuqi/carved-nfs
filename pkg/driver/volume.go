package driver

import (
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/container-storage-interface/spec/lib/go/csi"
	appsv1 "k8s.io/api/apps/v1"
	apiv1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/klog"
)

const (
	driverNamespace = "carymatt-carved-nfs"
	labelKey        = "carymatt/carved-nfs"
	nfsImage        = "k8s.gcr.io/volume-nfs:latest"
	mountBase       = "/mnt"    // Used for verifying volumes; should be emptyDir or similar in driver pod.
	volumePVCName   = "exports" // The template PVC name in the NFS server StatefulSet.
)

var (
	deletePolicy  = metav1.DeletePropagationForeground
	deleteOptions = metav1.DeleteOptions{
		PropagationPolicy: &deletePolicy,
	}
)

func validateCapabilities(caps []*csi.VolumeCapability) error {
	for _, c := range caps {
		if c.GetMount() == nil {
			return fmt.Errorf("driver only support mount access type volume capability")
		}
	}
	return nil
}

func statefulSetName(volumeId string) string {
	return volumeId + "-statefulset"
}

func statefulSetLabel(volumeId string) string {
	return volumeId + "-label"
}

func serviceName(volumeId string) string {
	return volumeId + "-svc"
}

type instance struct {
	nfs       NFS
	mountPath string
}

// newTempInstance creates an instance with the volume mounted to a temporary directory. disconnect() should be called to clean up.
func newTempInstance(volumeId string) (*instance, error) {
	server := serviceName(volumeId)
	mountPath, err := ioutil.TempDir(mountBase, "mount")
	if err != nil {
		return nil, fmt.Errorf("Bad temp dir: %w", err)
	}
	nfs := NewNFS(server, "/")
	if err := nfs.Mount(mountPath); err != nil {
		os.Remove(mountPath)
		return nil, fmt.Errorf("Bad mount for %s: %w", server, err)
	}
	return &instance{nfs, mountPath}, nil
}

func (i *instance) disconnect() {
	if i.nfs == nil || i.mountPath == "" {
		klog.Warning("Ignoring disconnect for empty instance")
		return
	}
	if err := i.nfs.UnMount(i.mountPath); err != nil {
		klog.Warningf("Ignored error when unmounting %s: %v", i.mountPath, err)
	}
	if err := os.Remove(i.mountPath); err != nil {
		klog.Warningf("Ignored error when removing mount path %s: %v", i.mountPath, err)
	}
	i.nfs = nil
	i.mountPath = ""
}

func (s *controllerServer) makeVolumePVCTemplate(capacity *resource.Quantity) *apiv1.PersistentVolumeClaim {
	return &apiv1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name: volumePVCName,
		},
		Spec: apiv1.PersistentVolumeClaimSpec{
			AccessModes: []apiv1.PersistentVolumeAccessMode{
				"ReadWriteOnce",
			},
			StorageClassName: Stringptr("standard-rwo"),
			Resources: apiv1.ResourceRequirements{
				Requests: apiv1.ResourceList{
					"storage": *capacity,
				},
			},
		},
	}
}

func (s *controllerServer) makeVolumeStatefulSet(volumeId string, reqBytes int64) *appsv1.StatefulSet {
	pvc := s.makeVolumePVCTemplate(resource.NewScaledQuantity(reqBytes, 0))
	return &appsv1.StatefulSet{
		ObjectMeta: metav1.ObjectMeta{
			Name: statefulSetName(volumeId),
		},
		Spec: appsv1.StatefulSetSpec{
			Replicas:    Int32ptr(1),
			ServiceName: serviceName(volumeId),
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{
					labelKey: statefulSetLabel(volumeId),
				},
			},
			VolumeClaimTemplates: []apiv1.PersistentVolumeClaim{
				*pvc,
			},
			Template: apiv1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: map[string]string{
						labelKey: statefulSetLabel(volumeId),
					},
				},
				Spec: apiv1.PodSpec{
					Containers: []apiv1.Container{
						{
							Name:  "nfs-server",
							Image: nfsImage,
							Args: []string{
								"/exports",
							},
							Ports: []apiv1.ContainerPort{
								{
									Name:          "nfs",
									ContainerPort: 2049,
								},
								{
									Name:          "mountd",
									ContainerPort: 20048,
								},
								{
									Name:          "sunrpc",
									ContainerPort: 111,
								},
							},
							SecurityContext: &apiv1.SecurityContext{
								Privileged: Boolptr(true),
							},
							VolumeMounts: []apiv1.VolumeMount{
								{
									MountPath: "/exports",
									Name:      volumePVCName,
								},
							},
						},
					},
				},
			},
		},
	}
}

func (s *controllerServer) makeVolumeService(volumeId string) *apiv1.Service {
	return &apiv1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name: serviceName(volumeId),
		},
		Spec: apiv1.ServiceSpec{
			Ports: []apiv1.ServicePort{
				{
					Name: "nfs",
					Port: 2049,
				},
				{
					Name: "mountd",
					Port: 20048,
				},
				{
					Name: "sunrpc",
					Port: 111,
				},
			},
			Selector: map[string]string{
				labelKey: statefulSetLabel(volumeId),
			},
		},
	}
}

func (s *controllerServer) getVolumePVCName(ctx context.Context, volumeId string) (string, error) {
	pod, err := s.clientset.CoreV1().Pods(driverNamespace).Get(ctx, statefulSetName(volumeId) + "-0", metav1.GetOptions{})
	if err != nil {
		return "", fmt.Errorf("Could not find server pod -0 for %s: %w", volumeId, err)
	}
	for _, volume := range pod.Spec.Volumes {
		if volume.Name == volumePVCName {
			if volume.PersistentVolumeClaim.ClaimName == "" {
				return "", fmt.Errorf("Missing PVC in server pod %s: %v", volumeId, volume)
			}
			return volume.PersistentVolumeClaim.ClaimName, nil
		}
	}
	return "", fmt.Errorf("Missing server volume for %s", volumeId)
}

func (s *controllerServer) createVolumeServer(ctx context.Context, volumeId string, reqBytes int64) error {
	okay := false
	svc := s.makeVolumeService(volumeId)
	_, err := s.clientset.CoreV1().Services(driverNamespace).Create(ctx, svc, metav1.CreateOptions{})
	if err != nil {
		return err
	}
	statefulSet := s.makeVolumeStatefulSet(volumeId, reqBytes)
	_, err = s.clientset.AppsV1().StatefulSets(driverNamespace).Create(ctx, statefulSet, metav1.CreateOptions{})
	if err != nil {
		return err
	}
	defer func() {
		if !okay {
			s.clientset.AppsV1().StatefulSets(driverNamespace).Delete(ctx, statefulSetName(volumeId), deleteOptions)
		}
	}()
	okay = true
	return nil
}

func (s *controllerServer) volumeServerExists(ctx context.Context, volumeId string) (bool, error) {
	_, err := s.clientset.CoreV1().Services(driverNamespace).Get(ctx, serviceName(volumeId), metav1.GetOptions{})
	if err == nil {
		return true, nil
	}
	if errors.IsNotFound(err) {
		return false, nil
	}
	return false, err
}

func (s *controllerServer) deleteVolumeServer(ctx context.Context, volumeId string) error {
	var errors []string
	if err := s.clientset.CoreV1().Services(driverNamespace).Delete(ctx, serviceName(volumeId), deleteOptions); err != nil {
		errors = append(errors, "service delete: "+err.Error())
	}
	if err := s.clientset.AppsV1().StatefulSets(driverNamespace).Delete(ctx, statefulSetName(volumeId), deleteOptions); err != nil {
		errors = append(errors, "statefulSet delete: "+err.Error())
	}
	pvcName, err := s.getVolumePVCName(ctx, volumeId)
	if err != nil {
		errors = append(errors, "getVolumePVCName: "+err.Error())
	} else {
		if err := s.clientset.CoreV1().PersistentVolumeClaims(driverNamespace).Delete(ctx, pvcName, deleteOptions); err != nil {
			errors = append(errors, fmt.Sprintf("PVC delete (%s): %s", pvcName, err.Error()))
		}
	}
	if errors != nil {
		return fmt.Errorf("Deleting NFS server for %s: %s", volumeId, strings.Join(errors, "; "))
	}
	return nil
}

func (s *controllerServer) listVolumes(ctx context.Context) ([]string, error) {
	svcs, err := s.clientset.CoreV1().Services(driverNamespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	var svcNames []string
	for _, s := range svcs.Items {
		svcNames = append(svcNames, s.Name)
	}
	return svcNames, nil
}

func Int32ptr(i int32) *int32    { return &i }
func Stringptr(s string) *string { return &s }
func Boolptr(b bool) *bool       { return &b }
