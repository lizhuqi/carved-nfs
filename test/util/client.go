package util

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
	"k8s.io/klog"
)

// PackageRoot returns the root of this package (gitlab.com/carymatt/carved-nfs) using GOPATH.
func PackageRoot() string {
	godir := os.Getenv("GOPATH")
	if godir == "" {
		godir = filepath.Join(homedir.Homedir(), "go")
	}
	return filepath.Join(godir, "src/gitlab.com/carymatt/carved-nfs/")
}

// ClientConfig holds configuration for a K8s client, either using kubectl or client-go.
type ClientConfig interface {
	KubectlApply(path string) error
	KubectlWaitCondition(kind, name, condition string) error
	GetClientset() (kubernetes.Clientset, error)
}

type clientConfig struct {
	kubeconfig string
	clientset  kubernetes.ClientSet
}

func NewClientConfig() (ClientConfig, error) {
	var kubeconfig *string
	if home := homedir.HomeDir(); home != "" {
		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}
	flag.Parse()

	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		return nil, err
	}
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}
	return &clientConfig{*kubeconfig, clientset}, nil
}

func (c *clientConfig) KubectlApply(path string) error {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return fmt.Errorf("Couldn't find %s to apply", path)
	}
	_, err := exec.Command("kubectl", "--kubeconfig", c.kubeconfig, "apply", "-f", path).CombinedOutput()
	return err
}

func (c *clientConfig) KubectlWaitCondition(kind, name, condition string) error {
	out, err := exec.Command("kubectl", "--kubeconfig", c.kubeconfig, "wait", kind, name,
		fmt.Sprintf("--for=condition=", condition)).CombinedOutput()
	// TODO: checkout output to see if wait succeeded.
	klog.Infof("Wait for %s: %v", name, out)
	return err
}

func (c *clientConfig) GetClientset() (kubernetes.Clientset, error) {
	if c.clientset == nil {
		return fmt.Errorf("ClientConfig uninitialized")
	}
	return c.clientset, nil
}
