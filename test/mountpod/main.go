package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/carymatt/carved-nfs/pkg/driver"
	"k8s.io/klog"
)

func init() {
	klog.InitFlags(flag.CommandLine)
}

func main() {
	flag.Parse()

	nfsRoot := driver.NewNFS("nfs-service.default", "/")
	rootShare := "/data/rootShare"
	if err := os.Mkdir(rootShare, 0755); err != nil {
		klog.Fatalf("Can't make root share: %w", err)
	}
	if err := nfsRoot.Mount(rootShare); err != nil {
		klog.Fatalf("Mount of root failed: %w", err)
	}
	testName := filepath.Join(rootShare, fmt.Sprintf("%d", time.Now().Unix()))
	if err := ioutil.WriteFile(testName, []byte("test data"), 0644); err != nil {
		klog.Fatalf("Failed to write test data: %w", err)
	}
	if err := nfsRoot.UnMount(rootShare); err != nil {
		klog.Fatalf("Unmount of root failed: %w", err)
	}
	if _, err := os.Stat(testName); !os.IsNotExist(err) {
		klog.Fatalf("Expected test data to disappear after unmount: %w", err)
	}
	if err := nfsRoot.Mount(rootShare); err != nil {
		klog.Fatalf("Remount of root failed: %w", err)
	}
	if _, err := os.Stat(testName); err != nil {
		klog.Fatalf("Expected stat of test data after remount: %w", err)
	}
	klog.Info("mount test complete!")
}
