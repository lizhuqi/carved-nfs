# CarvedNFS Benchmarking

This directory hsa kubernetes configuration for benchmarking experiments with
carved NFS. The benchmarks run with a prometheus instance which will display the
IO performance of the benchmarking apps.

Follow the instructions in [/README.md][/README.md] to deploy (ie, `kubectl apply -f
deploy/driver`).

To run, first

```
kubectl apply -f ./setup
```

to deploy prometheus. Connect to the prometheus UI by first

```
kubectl get svc
```

and note the `NodePort` of the prometheus service. Then

```
gcloud compute firewall-rules create prometheus --allow tcp:$NODEPORT
```

where `$NODEPORT` is from the prometheus service. Run

```
gcloud compute instances list
```

to get the nodes, and connect with your web browser to any
`$EXTERNAL_IP:$NODEPORT` to access the prometheus web UI. Enter a query like

```
{__name__=~"readbw|writebw"}
```

and select graphs. This will show the read and write bandwidth (in K/s) of fio
pods which have not been started yet.

```
kubectl apply -f ./workloads/fio-a-carved-nfs.yaml
kubectl apply -f ./workloads/fio.yaml
```

to deploy 10 fio pods in a StatefulSet, starting at 2 minute intervals. These
run a read/write benchmark on the same carved-nfs PVC. Watch on the prometheus
graphs how the bandwidths changes as the pods go through the same NFS server.

There are also PD and filestore PVCs to use instead. The Filestore PVC yaml also
declares a PV that must be pointed to a Filestore instance; there are gcloud
instructions in comments in that file on how to do that. For pd, there is a
combined StatefulSet and PVC deployment in `fio-pd.yaml` as the PD can only have
one instance.

## Results

### Bandwidth

10 pods, 10G PD. Write b/w starts at 774 K/s, then drops in steps to ~200. Peaks as pods
come on, by 50% at start decreasing to ~10%; overall b/w trails to 140 after all
pods online. Read b/w starts at 9k, quickly drops to 740, same bumps as pods
come online, tails out to 200.

With a 100G PD, same pattern, tailing to 500 write b/w & 750 read b/w.

In both cases, CPU on the NFS server pod is negligible, and memory use 150-200M.

1T PD, 4.2k / 6.6k after one pod; 5.1k / 7.6k after 5; the same after 7.

Using a single pod directly on a PD, the write b/w trails to 140 and the read to
200, identically (ie there's no overhead lost due to the NFS server).

With a 100G PD, trails to 287 write b/w and 430 read b/w, significantly lower
than with NFS. Weird.

1T PD, 2.6k write and 4k read.

Filestore! 1T volume. 6k write, 9k read with one pod; 12k / 15k after two;
13k/19k after 3; 14k/22k after 4...

### Latency

All latencies below are in microseconds.

10 pods carved nfs, latency on first pod starting at ~5k increasing to 13k by
pod 5. Other pods, probably not on the same node, have nearly double the latency
(~20k).

1 pod PD, converging quickly to 9k

Looking at min total latency instead, there is a lot of variations apparently
depending on where things are scheduled. The first carved-nfs pods 1800 read /
600 write, but subsequent pods got up to 200k+. Starting the PD job
simultaneously had 10k read / 10k write, but after killing the carved NFS
StatefulSet it dropped to 2k read / 100 write.

So latency is higher with carved NFS, but also very complicated and depends a
lot on the PD configuration.

For filestore minimum latencies, the first pod starts at 320 read / 132
write. The second pod was faster, 238 read / 83 write. After a couple of
minutes, the first pod latencies dropped to that of the second. A third pod hit
420 read / 100 write, subsequent pods dropped down to that level as well.

## Failure Recovery

Start `fio-a-carved-nfs.yaml` and then `fio.yaml`. Wait for the jobs
to warm up and get going, verifying on prometheus that they look
healthy.

Log into a fio task and start writing to a file:

```
kubectl exec fio-a-0 -ti -- bash
for ((i=0;i<100000;i++)); do echo $i >> /data/count; done
```

Keeping the above running, log into a different task and tail that file.

```
kubectl exec fio-a-1 -ti -- bash
tail -f /data/count
```

Verify that the counts are increasing. Keep this running too.

Kill the carved-nfs PVC server pod to simulate a failure.

```
kubectl get pod -n carymatt-carved-nfs
kubectl delete pod -n carymatt-carved-nfs $pvc-XXXX-seen-from-previous-command
```

Return to the tailing shell, and watch the counts pause while the PVC
server pod is restarted. Some of the fio jobs may fail, but they seem
to be able to restart. In a couple minutes the counting should resume
as well.

It seems to get a bit rocky with the 10 fio tasks and maybe can get
wedged. If instead this is run with `deploy/test/driverpod.yaml` and
`.../driverpod2.yaml`, it seems to work more consistently.

## Questions

* Why does the bandwidth begin high, then drop? PD throttling?
* What exactly happens from the workload perspective with the PVC
  server goes down? Do file operations fail for a bit and then start
  succeeding? Is it possible for reads to hang?
