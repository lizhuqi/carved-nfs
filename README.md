# Stacked NFS CSI Driver for Kubernetes

This is a proof-of-concept for a Kubernetes dynamically provisioned
zero-configuration multi-writer volume CSI driver. The goal is to have a
reliable and performant multi-writer volumes in kubernetes that does require
additional administration such as setting up a separate NFS server.

This does not aim to replace production-grade products such as Filestore or
EFS. It is meant for development and testing of Kubernetes
applications. Its architecture provides for isolation between
applications, as all PVs are independent. It suffers from a single
point of failure in the NFS server. Basic testing has shown that the
volume and workloads using that volume can recover from this server
failing.

(Note that I've been inconsistent in naming, sometimes I call it
"carved NFS" and sometimes "stacked NFS". Sorry...)

## Architectural Overview

A stacked NFS PersistentVolume is an NFS server pod in the driver namespace
attached to block storage (ie, a ReadWriteOnce volume). These are done at volume
creation. Node publishing is done by contacting that server to mount as an NFS
device. All stacked NFS PVs are independent (one NFS server per PV).

```
                                        +------------------------------+
+------------------------------------+  | Consumer Node                |
| Driver Namespace                   |  |                              |
|                                    |  | +--------------+             |
| +--------------------+   +-------+ |  | | Workload Pod |             |
| | StatefulSet Pod    |   | PV I  | |  | |              |             |
| | running NFS server |   +-------+ |  | | Volume...........NFS Mount |
| |                    |      |      |  | +----|---------+     |       |
| |                    |   +-------+ |  +------|---------------|-------+
| |             Volume-----| PVC I | |     +---|---+           |
| +--------------------+   +-------+ |     | PVC C |           |
|          |                         |     +---|---+           |
|     +---------+                    |         |               |
|     | Service |<-----------\       |     +---|---+           |
|     +---------+             \------------| PV C  |           |
|          ^                         |     +-------+           |
+----------|-------------------------+                         |
           \---------------------------------------------------/
```

Above, the Consumer PVC should refer to a StorageClass with provisioner
carved-nfs.carymatt.gitlab.com. This will create as appropriate the Internal
PV and PVC along with a StatefulSet running an NFS server and an associated
service.

Note that PV C actually refers to an abstract volume name, not the NFS
service specifically. At mount time the service is recovered up based
on this name. When the volume is published for a pod on a node, an NFS
mount is made using the internal service.

Multiple PVs will each create a Pod, Service, PD-backed PV and a PVC.

## Usage

See [deploy/test/driverpod.yaml](./deploy/test/driverpod.yaml) for a usage
example using the provided carved-nfs storageclass. This creates a PVC which
will dynamically allocate a stacked NFS
PV. [deploy/test/driverpod2.yaml](./deploy/test/driverpod2.yaml) can then be
brought up which will attach to the same PVC. Note you will have to change the
image repo which currently point to the author's private repo.

With

```
kubectl ssh driverpod -- 'echo "hello #2!" > /data/hello'
kubectl ssh driverpod2 -- cat /data/hello
```

one can demonstrate the glorious splendour of ReadWriteMany volumes in
kubernetes.

## Deployment

The current implementation assumes running on a cluster with a `standard-rwo`
StorageClass, for example as provided by a modern CSI driver. `make
cluster` will create a compatible GKE cluster using the
GcePersistentDiskCsiDriver addon.

There is no public repository for this driver's images, so you will need to
build and push them to a private repo. The makefile here provides support for a
gcr.io repo with a GCP project.

`PROJECT=your-gcp-project make driver` to build and push up the image. Then edit
images in `deploy/driver/*.yaml` to match your project, and `kubectl apply -f
deploy/driver` to install the driver.

## Benchmarking & Robustness

See notes in [deploy/benchmark/README.md](./deploy/benchmark/README.md) for
running some light performance benchmarks. This also serves as a more complete
usage example.

There is also a description of what happens if the PVC server pod is
killed while jobs are using the stacked volume. Everything basically
continues to work, although I suspect it relies on workloads
intelligently handling file errors.

## Development Status

This is a pre-alpha, proof-of-concept driver. It is not
production-quality. In particular if the internal volume NFS server
restarts, consumer pods probably will hang (this is not yet
well-tested). Extending the driver to gracefully remount in that case
would be a welcome extension to the current implementaton.

The plumbing for CSI features such as volume expansion and snapshots has not
been done, although conceptually it is straightforward to forward those requests
through to the internal PV.
